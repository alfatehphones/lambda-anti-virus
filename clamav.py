from common import AV_DEFINITION_FILE_PREFIXES
from common import AV_DEFINITION_FILE_SUFFIXES


def defs_files():
    for prefix in AV_DEFINITION_FILE_PREFIXES:
        for suffix in AV_DEFINITION_FILE_SUFFIXES:
            yield "{}.{}".format(prefix, suffix)


for file in defs_files():
    print(file)
