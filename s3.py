import boto3
import botocore
import os


def md5_from_tags(bucket, key, s3_client=None):
    try:
        if not s3_client:
            s3_client = boto3.client("s3")

        tags = s3_client.get_object_tagging(Bucket=bucket, Key=key)["TagSet"]
    except botocore.exceptions.ClientError as e:
        expected_errors = {
            "404",  # Object does not exist
            "AccessDenied",  # Object cannot be accessed
            "NoSuchKey",  # Object does not exist
            "MethodNotAllowed",  # Object deleted in bucket with versioning
        }
        if e.response["Error"]["Code"] in expected_errors:
            return ""
        else:
            raise

    for tag in tags:
        if tag["Key"] == "md5":
            return tag["Value"]

    return ""


def upload_file(bucket, prefix, local_path, filename, s3=None):
    key = os.path.join(prefix, filename)

    print("Uploading {} to s3://{}/{}".format(local_path, bucket, key))

    if not s3:
        s3 = boto3.resource("s3")

    s3_object = s3.Object(bucket, key)
    s3_object.upload_file(os.path.join(local_path, filename))


def tag_object(bucket, key, tag, value, s3_client=None):
    if not s3_client:
        s3_client = boto3.client("s3")

    s3_client.put_object_tagging(
        Bucket=bucket,
        Key=key,
        Tagging={"TagSet": [{"Key": tag, "Value": value}]}
    )
