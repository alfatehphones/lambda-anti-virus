# Lambda Anti-Virus Scanner

Lambda based ClamAV anti-virus scanner for S3 files. Based on the ideas in https://github.com/upsidetravel/bucket-antivirus-function.

ClamAV definitions are stored in an S3 bucket and updated on a 3 hourly schedule using FreshClam running in the `update` lambda function.

One or more addiitonal S3 buckets are configured with a event trigger for new objects running the `scan` lambda function. This uses the ClamAV definitions from S3 to run a ClamScan on the newly created file. Results of the scan are available in CloudWatch only at this time.

## ClamAV Lambda Layer

The ClamScan and FreshCalm binaries are built and linked on AmazonLinux 2 using docker exported to a Lambda layer for use by multiple functions.

Run `make all` to build the layer in the `clamav-layer` directory. This is then deployed to AWS using `serverless`.

## Update function

The update function updates the anti-virus definitions stored in S3 and used by ClamScan to run scans. Update utilises FreshClam to get the latest definition files.

Once downloaded (and to save bandwidth) the local definitions are checked against the MD5 sum for the current definition files stored in S3 and only updated if there is a mismatch.

The update function is deployed and its schedule configured using serverless.

```bash
sls deploy -f update
```

## Scan function

The scan function is triggered by S3 create object events on configured buckets. This is deployed using serverless and utilises the same ClamAV layer as the update function.

To allow existing buckets (those not created by serverless) the `serverless-plugin-existing-s3` plugin is used.

```bash
sls deploy -f scan
sls s3deploy -f scan
```

## Intial Deployment

When deploying for the first time or deploying changes to the ClamAV layer you must run a full serverless deployment.

```bash
make all
sls deploy
```

## Testing

The `test_files` directory contains three files from [eicar](https://www.eicar.org/?page_id=3950) that can be uploaded to a bucket configured for scans. Each file should get a positive result for an infection as they contain the `EICAR-STANDARD-ANTIVIRUS-TEST-FILE` signature.

## TODO & Future Enhancements

- Logging
- Configurable bucket names
- Improved logic for pulling defs in scan functions to use existing ones if on disk when available and current
- Do something more with results than just sending to CloudWatch
- Full unit and integration tests
